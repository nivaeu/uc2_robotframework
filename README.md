# Introduction
This sub-project is part of the ["New IACS Vision in Action” --- NIVA](https://www.niva4cap.eu/) project that delivers a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to support further development of IACS that will facilitate data and information flows. This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 842009. Please visit the [website](https://www.niva4cap.eu) for further information. A complete list of the sub-projects made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

**Robot Framework (in NIVA project also known as Robot Data Harvesting Tool)**

Robot Framework is an open-source RPA solution for acceptance testing, acceptance test-driven development (ATDD), and used to automate business processes. It is open and extensible, therefore it can be integrated with any other tool in order to create even more powerful and flexible RPA solutions. 

The core of Robot Framework, the generic automation platform, is very popular and has an active community of developers and users in many industry-leading companies, which uses it in their software development. This leads to active and continuous support and improvements. The community works actively toward additional functionality, like optical image recognition, HTTP APIs, database access, remote execution, and iOS and Android application support, while orchestration functionality in commercial projects is mostly available through Jenkins plugins. 

The Robot Framework has a simple plain text syntax, which can be extended by libraries using Java or Python. This enables any user to integrate new functionality into the Robot Framework. 

Because Robot Framework is open source, it is free to use without any licensing costs and users can run any number of Robot Framework RPA processes in parallel. This enables even small enterprises to automate some of their tasks with limited costs. 

**Architecture description and technology stack**

The Robot Framework utilizes a human-readable syntax which is based on keywords. Keywords are composable, meaning you can define new keywords that use pre-existing keywords provided by the libraries to interact with the target system. Libraries can communicate with the system either directly or using other tools as drivers.

**System requirements**

Robot Framework is an operating system and application independent.

**Hardware requirements**

The prototype has no hardware interfaces and has no constraints on hardware architecture.

**Software**

In order to perform demo tasks, these software configurations were used:
Python 3.8.2;
Robot Framework 3.1.2;
RIDE - Standalone Robot Framework test data editor.

**Installation**

Original Robot Framework installation can be found on Github: https://github.com/robotframework/robotframework


**Robot Framework use case (configured as "Nemiga.robot" in GITLAB repository)**

The Robot Framework was intended to apply in a test case for the Lithuanian Payment Agency (NMA). NMA is dealing with farmers’ applications, which are managed within the internal NMA system. During this process, experts define certain parameters and manually check and compare various data between different systems, external registers, and other data storage. For demo and test purposes, following tasks for Robot Framework were addressed:
1.To edit data of certified fields in the PPAIS system (Lithuanian application’s administration system in NMA) depending on certificate data of Ekoagros website. The robot has to check the certificate data in Ekoagros website (an external data repository) and based on that, edit specific fields in the internal system.
2.To save certain certificates in HTML format in a specific directory in the hard drive. 
3.Based on certification information, change farmers’ applications’ attributes if needed.
The code of this demo scenario is saved in the Nemiga.robot file and can be executed by Robot Framework.
