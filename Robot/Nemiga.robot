*** Settings ***
Suite Setup       Close All Browsers
Suite Teardown
Test Setup        Add Needed Image Path
Test Teardown     Stop Remote Server
Library           SeleniumLibrary    # Web testing library that uses popular Selenium tool internally.
Library           SikuliLibrary    # Sikuli Robot Framework Library provide keywords to test UI through Sikulix. This library supports Python 2.x and 3.x.
Library           DateTime    # Standard library for date and time conversions.
Library           OperatingSystem    # Standard library. Enables various operating system related tasks to be performed in the system where Robot Framework is running.
Library           OracleDB    # Library for working with Oracle database, using cx_Oracle.
Library           ExcelRobot    date_format='yyyy-mm-dd'    # Library for opening, reading, writing, and saving Excel files from Robot Framework.
Library           Collections    # Standard library. Provides a set of keywords for handling Python lists and dictionaries.
Library           Dialogs    # Standard library. Provides means for pausing the execution and getting input from users.
Resource          config.resource    # List of preset variables
Resource          logins.resource    # Collection of addresses, usernames, passwords needed for test suite.

*** Variables ***
${reg_numeris}    \    # Reg. number
${prs_id}         ${EMPTY}    # Application ID
${prs_numeris}    ${EMPTY}    # Application number
${query2}         ${EMPTY}
${q2status}       ${EMPTY}
${q2value}        ${EMPTY}
@{blokai}         # Array of blocks
@{laukai}         # Array of fields
${index}          ${EMPTY}
${qstatus}        ${EMPTY}
${kiek_lauku}     ${EMPTY}    # Number of fields
@{sert_plotai}    # Certified fields
@{lauko_statusai}    # Field statuses
@{augalai}
@{pastabos}
${open_ppais}     0
${pastabos}       ${EMPTY}
${open_agros}     0

*** Test Cases ***
TC_001_Agros
    ${chromeOptions}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    ${prefs} =    Create Dictionary    download.prompt_for_download=${TRUE}
    Call Method    ${chromeOptions}    add_experimental_option    prefs    ${prefs}
    Create Webdriver    Chrome    chrome_options=${chromeOptions}
    Go To    ${UrlAgros}
    Wait Until Page Contains Element    name=username
    SeleniumLibrary.Input Text    name=username    ${userAgros}
    SeleniumLibrary.Input Password    name=password    ${passAgros}
    Click Button    //input[@value='Prisijungti']
    Wait Until Page Contains Element    name=EARLIEST
    SeleniumLibrary.Input Text    name=EARLIEST    ${agrosNuo}
    SeleniumLibrary.Input Text    name=LATEST    ${agrosIki}
    Click Button    //input[@value='Filtruoti']
    Sleep    2s
    Click Element    //img[@alt='eksportuoti i Excel']
    Sleep    2s
    Paste Text    \    ${listpath}\\ekoagros_${metai}_${agrosNuo}_${agrosIki}.xls
    Press Special Key    ENTER
    Click Button    //input[@value='Rodyti visus']

TC_002_MAIN
    Close All Browsers
    Open Excel    ${listpath}\\ekoagros_${metai}_${agrosNuo}_${agrosIki}.xls
    ${rowNum}    Get Row Count    Sheet1
    ${met_metai} =    Set Variable    2018
    ${ekoagros}    Set Variable    ROB_EKOAGROS
    Log    ${rowNum}
    Open Excel To Write    ${listpath}\\ekoagros_${metai}_${agrosNuo}_${agrosIki}.xls    ${listpath}\\done\\ekoagros_${metai}_${agrosNuo}_${agrosIki}.xls    override=True
    Connect To Oracle    172.20.14.221/ppstest    nivarob    nivarob    ppais
    OracleDB.Execute Plsql Block    alter session set current_schema \= pps
    Set Min Similarity    0.68
    FOR    ${row}    IN RANGE    1    10
        @{rowData} =    Get Row Values    Sheet1    ${row}    include_empty_cells=True
        Log Many    @{rowData}
        ${stcell_empty} =    Run Keyword And Return Status    Should Be Empty    ${rowData[13][1]}
        Run Keyword Unless    ${stcell_empty}    Continue For Loop
        ${reg_numeris} =    Set Variable    ${rowData[1][1]}
        ${status}    ${value} =    Run Keyword And Ignore Error    Should Match Regexp    ${reg_numeris}    -18-
        ${rowc}    Evaluate    ${row}+1
        ${valda} =    Set Variable    ${rowData[8][1]}
        Run Keyword Unless    '${status}' == 'PASS'    Run Keywords    Write To Cell By Name    Sheet1    N${rowc}    Neaktualus
        ...    AND    Continue For Loop
        Run Keyword If    '${rowData[12][1]}' == 'Taip'    Run Keywords    Write To Cell By Name    Sheet1    N${rowc}    Neaktualus
        ...    AND    Continue For Loop
        @{queryResults} =    Run First Query    ${valda}    ${met_metai}    ${ekoagros}
        ${qstatus}    ${qvalue} =    Run Keyword And Ignore Error    Should Be Empty    ${queryResults}
        Run Keyword If    '${qstatus}' == 'PASS'    Run Keywords    Write To Cell By Name    Sheet1    N${rowc}    Nerasta valda ${met_metai}
        ...    AND    Continue For Loop
        ${prs_busena}    Set Variable If    '${qstatus}' == 'FAIL'    ${queryResults[0][2]}
        ${prs_zuikvc_id}    Set Variable If    '${qstatus}' == 'FAIL'    ${queryResults[0][7]}
        Run Keyword If    '${qstatus}' == 'FAIL' and '${prs_busena}' == 'AT'    Run Keywords    Write To Cell By Name    Sheet1    N${rowc}    Paraiška atmesta
        ...    AND    Continue For Loop
        ${q2status} =    Run Keyword If    '${qstatus}' == 'FAIL' and '${prs_busena}' != 'AT'    Run Second Query    ${queryResults[0][0]}    ${queryResults[0][1]}    ${ekoagros}    ${rowc}
        Log    ${q2status}
        Run Keyword If    '${q2status}' == 'PASS'    Run Keywords    Write To Cell By Name    Sheet1    N${rowc}    Suimportuota anksčiau
        ...    AND    Continue For Loop
        ${prs_numeris}    Set Variable    ${queryResults[0][1]}
        Run Keyword If    '${q2status}' == 'FAIL'    Paruosti paraiska    ${prs_numeris}    ${reg_numeris}    ${row}
        Run Keyword Unless    '${prs_busena}' in ${busenos}    Pastaba Netinkama Busena    ${reg_numeris}    ${prs_busena}
        @{blokai_laukai}    ${kiek_lauku}    Run Keyword    Gauti Sertifikato Israsa    ${prs_numeris}    ${reg_numeris}
        ${len}    Get Length    ${blokai_laukai}
        Execute Manual Step    Switch to PPAIS
        Run Keyword If    '${len}'=='0'    Registruoti Paraiskos Pozymi    ${ekoagros}    Taip    ${reg_numeris}
        ${pstatus}    ${pvalue} =    Run Keyword And Ignore Error    Screen Should Contain    btnPrisiskirti.png
        Run Keyword If    '${pstatus}'=='PASS'    Click    btnPrisiskirti.png
        Sleep    2s
        ${wstatus}    ${wvalue} =    Run Keyword And Ignore Error    Screen Should Contain    pranesimai_perspejimai.png
        Run Keyword If    '${pstatus}'=='PASS' and '${wstatus}'=='PASS'    Run Keywords    Click    btnTesti.png
        ...    AND    Wait Until Screen Contain    tvarkyti_paraiskas.png    10
        #    Run Keyword Unless    '${prs_busena}'=='NG'    Uzregistruoti Nagrinejima
        Run Keyword If    '${len}'!='0'    Registruoti Paraiskos Pozymi    ${ekoagros}    Taip    0
        #    Click    btnDuomenys.png
        #    Wait Until Screen Contain    tvarkyti_pdd.png    30
        ${pastabos} =    Tikrinti Laukus    ${prs_zuikvc_id}    ${blokai_laukai}    ${kiek_lauku}    ${ekoagros}
        ${len}    Get Length    ${pastabos}
        Run Keyword If    '${len}'!='0'    Lauku Pastabos    ${pastabos}
        Run Keyword If    '${len}'!='0'    Run Keywords    Write To Cell By Name    Sheet1    N${rowc}    Suimportuota su klaidomis
        ...    AND    Continue For Loop
        Run Keyword If    '${len}'=='0'    Write To Cell By Name    Sheet1    N${rowc}    Suimportuota
        Save Excel
    END
    Save Excel
    Close All Oracle Connections

html
    Open Browser    ${UrlAgros}    ie
    Maximize Browser Window
    ${ekoagros}    Set Variable    EKOAGR
    Wait Until Page Contains Element    name=username
    SeleniumLibrary.Input Text    name=username    ${userAgros}
    SeleniumLibrary.Input Password    name=password    ${passAgros}
    Click Button    //input[@value='Prisijungti']
    Wait Until Page Contains Element    name=EARLIEST
    Clear Element Text    name=EARLIEST
    Clear Element Text    name=LATEST
    SeleniumLibrary.Input Text    name=DOC_SERIAL    NUS-U-15-02620
    Click Button    //input[@value='Filtruoti']
    Sleep    3s
    ${statusVisible}    ${valueVisible} =    Run Keyword And Ignore Error    Element Should Be Visible    xpath://img[@alt='peržiūrėti']
    ${count}    Set Variable    0
    ${count} =    Run Keyword If    '${statusVisible}' == 'PASS'    Get Element Count    xpath://img[@alt='peržiūrėti']
    ...    ELSE    Set Variable    0
    ${status}    ${value} =    Run Keyword And Ignore Error    Should Be True    '${count}' == '1'
    Run Keyword Unless    '${status}' == 'PASS'    Log    ALGKLAIDA: ${count} įrašų
    Run Keyword If    '${status}' == 'PASS'    Click Element    //img[@alt='peržiūrėti']
    Wait Until Element Is Visible    //form[@id='quickform']    10
    ${src}=    Get Source
    Create File    ${sertpath}\\${prs_numeris}_ekoagros_${reg_numeris}.htm    ${src}
    ${rows} =    Get Element Count    xpath://div[@id='EditableGrid-FIELDS_DES']//table//tr
    Log    ${rows}
    Create List    @{blokai}
    Create List    @{laukai}
    FOR    ${row}    IN RANGE    2    ${rows}+1
        ${blokas} =    Get Table Cell    xpath://div[@id='EditableGrid-FIELDS_DES']//table    ${row}    1
        ${laukas} =    Get Table Cell    xpath://div[@id='EditableGrid-FIELDS_DES']//table    ${row}    2
        Append To List    ${blokai}    ${blokas}
        Append To List    ${laukai}    ${laukas}
    END
    @{blokai_laukai}    Create List    ${blokai}    ${laukai}
    Log Many    @{blokai_laukai}
    Connect To Oracle    192.168.200.212:1521/ppsb    pps    pps    ppais
    FOR    ${row}    IN RANGE    0    ${rows}-1
        ${blokas} =    Set Variable    ${blokai_laukai[0][${row}]}
        ${laukas} =    Set Variable    ${blokai_laukai[1][${row}]}
        Log    ${blokas}
        Log    ${laukas}
        @{query3Results} =    Rasti Lauka    1308519    ${blokas}    ${laukas}    ${ekoagros}    ${ekoagros}    ${ekoagros}
        Log Many    @{query3Results}
    END
    Close All Oracle Connections

*** Keywords ***
Add Needed Image Path
    Add Image Path    ${ImageDir}

Run First Query
    [Arguments]    ${valda}    ${met_metai}    ${ekoagros}
    ${query} =    Get File    ${dbpath}\\u-paraiska.sql
    @{queryResults} =    OracleDB.Execute Sql String    ${query}    arg1=${valda}    arg2=${met_metai}    arg3=${ekoagros}
    Log Many    @{queryResults}
    [Return]    @{queryResults}

Run Second Query
    [Arguments]    ${prs_id}    ${prs_numeris}    ${ekoagros}    ${rowc}
    ${query2} =    Get File    ${dbpath}\\u-admpzmprs.sql
    @{query2Results} =    OracleDB.Execute Sql String    ${query2}    arg4=${ekoagros}    arg5=${prs_id}
    Log Many    @{query2Results}
    ${q2status}    ${q2value} =    Run Keyword And Ignore Error    Should Not Be Empty    ${query2Results}
    [Return]    ${q2status}

Run Skyriai Query
    [Arguments]    ${drb_username}    # Darbuotojo naudotojo vardas
    ${guery}    Get File    ${dbpath}\\u-skyriai.sql
    @{queryResults} =    Query    ${guery}
    Log Many    @{queryResults}

Paruosti paraiska
    [Arguments]    ${prs_numeris}    ${reg_numeris}    ${row}
    ${date} =    Get Current Date    result_format=%Y-%m-%d
    Run Keyword If    '${open_ppais}'=='0'    Open PPAIS
    Run Keyword If    '${open_ppais}'=='1'    Open Browser    browser=ie    alias=ppais
    SikuliLibrary.Wait Until Screen Contain    tvarkyti_paraiskas.png    60
    Press Special Key    F7
    SikuliLibrary.Paste Text    \    ${prs_numeris}
    Press Special Key    F8
    SikuliLibrary.Wait Until Screen Contain    areaDokumentai.png    20
    SikuliLibrary.Click In    areaDokumentai.png    btnDokumentai.png
    SikuliLibrary.Wait Until Screen Contain    tvarkyti_gaunamus.png    40
    Press Special Key    F6
    Sleep    1s
    SikuliLibrary.Input Text    \    A10
    Press Special Key    TAB
    SikuliLibrary.Input Text    \    ${date}
    Press Special Key    TAB
    Press Special Key    TAB
    Press Special Key    TAB
    SikuliLibrary.Input Text    \    1
    SikuliLibrary.Wait Until Screen Contain    tabKita.png    20
    SikuliLibrary.Click    tabKita.png
    ${nuorodaDVS}=    Catenate    SEPARATOR=    ${sertpath}    \\    ${prs_numeris}    _ekoagros_    ${reg_numeris}    .htm
    SikuliLibrary.Wait Until Screen Contain    areaDVS.png    20
    SikuliLibrary.Click In    areaDVS.png    dvs_num.png
    SikuliLibrary.Wait Until Screen Contain    dvs_num2.png    20
    SikuliLibrary.Paste Text    dvs_num2.png    ${reg_numeris}
    Press Special Key    TAB
    SikuliLibrary.Wait Until Screen Contain    dvs_nuoroda2.png    20
    SikuliLibrary.Paste Text    \    ${nuorodaDVS}
    Press Special Key    F10
    SikuliLibrary.Wait Until Screen Contain    kontr_klausimai.png    20
    SikuliLibrary.Click In    grieztas_ats.png    grieztas_ats1.png
    SikuliLibrary.Input Text    \    Taip
    SikuliLibrary.Click In    grieztas_ats.png    grieztas_ats2.png
    SikuliLibrary.Input Text    \    Taip
    Press Special Key    F10
    SikuliLibrary.Wait Until Screen Contain    btnPriimti.png    20
    SikuliLibrary.Click    btnPriimti.png
    Sleep    2s
    ${status}    ${value} =    Run Keyword And Ignore Error    Screen Should Contain    pranesimai_perspejimai.png
    Run Keyword If    '${status}'=='PASS'    Click    btnUzdaryti.png
    Type With Modifiers    q    CTRL

Pastaba Netinkama Busena
    [Arguments]    ${reg_numeris}    ${prs_busena}
    SikuliLibrary.Wait Until Screen Contain    tabs_prs.png    10
    SikuliLibrary.Click In    tabs_prs.png    tabPastabos.png
    SikuliLibrary.Wait Until Screen Contain    areaPastabos.png    10
    SikuliLibrary.Click    areaPastabos.png
    SikuliLibrary.Wait Until Screen Contain    areaPastabosAct.png    10
    Press Special Key    DELETE
    ${pastaba} =    Catenate    \#ROBOTAS\# sertifikatas    ${reg_numeris}    netinkama EKOAGROS tikrinimui paraiškos būsena    ${prs_busena}
    SikuliLibrary.Paste Text    areaPastabosAct.png    ${pastaba}
    Press Special Key    F10

Gauti Sertifikato Israsa
    [Arguments]    ${prs_numeris}    ${reg_numeris}
    Run Keyword If    '${open_agros}'=='0'    Open AGROS
    Run Keyword If    '${open_agros}'=='1'    Open Browser    browser=ie    alias=agros
    Go To    ${UrlAgros}
    Wait Until Page Contains Element    name=EARLIEST
    Clear Element Text    name=EARLIEST
    Clear Element Text    name=LATEST
    SeleniumLibrary.Input Text    name=DOC_SERIAL    ${reg_numeris}
    Click Button    //input[@value='Filtruoti']
    Sleep    3s
    ${statusVisible}    ${valueVisible} =    Run Keyword And Ignore Error    Element Should Be Visible    xpath://img[@alt='peržiūrėti']
    ${count}    Set Variable    0
    ${count} =    Run Keyword If    '${statusVisible}' == 'PASS'    Get Element Count    xpath://img[@alt='peržiūrėti']
    ...    ELSE    Set Variable    0
    ${status}    ${value} =    Run Keyword And Ignore Error    Should Be True    '${count}' == '1'
    Run Keyword Unless    '${status}' == 'PASS'    Log    ALGKLAIDA: ${count} įrašų
    Run Keyword If    '${status}' == 'PASS'    Click Element    //img[@alt='peržiūrėti']
    Wait Until Element Is Visible    //form[@id='quickform']    10
    ${src}=    Get Source
    Create File    ${sertpath}\\${prs_numeris}_ekoagros_${reg_numeris}.htm    ${src}
    ${kiek_lauku} =    Get Element Count    xpath://div[@id='EditableGrid-FIELDS_DES']//table//tr
    Log    ${kiek_lauku}
    Create List    @{blokai}
    Create List    @{laukai}
    Create List    @{sert_plotai}
    Create List    @{lauko_statusai}
    Create List    @{augalai}
    @{blokai_laukai}    Create List    ${blokai}    ${laukai}    ${sert_plotai}    ${lauko_statusai}    ${augalai}
    Log Many    @{blokai_laukai}
    Return From Keyword If    '${kiek_lauku}' == '1'    @{blokai_laukai}    ${kiek_lauku}
    FOR    ${row}    IN RANGE    2    ${kiek_lauku}+1
        ${blokas} =    Get Table Cell    xpath://div[@id='EditableGrid-FIELDS_DES']//table    ${row}    1
        ${laukas} =    Get Table Cell    xpath://div[@id='EditableGrid-FIELDS_DES']//table    ${row}    2
        ${sert_plotas} =    Get Table Cell    xpath://div[@id='EditableGrid-FIELDS_DES']//table    ${row}    4
        ${lauko_statusas} =    Get Table Cell    xpath://div[@id='EditableGrid-FIELDS_DES']//table    ${row}    6
        ${augalas} =    Get Table Cell    xpath://div[@id='EditableGrid-FIELDS_DES']//table    ${row}    7
        Append To List    ${blokai}    ${blokas}
        Append To List    ${laukai}    ${laukas}
        Append To List    ${sert_plotai}    ${sert_plotas}
        Append To List    ${lauko_statusai}    ${lauko_statusas}
        Append To List    ${augalai}    ${augalas}
    END
    [Return]    @{blokai_laukai}    ${kiek_lauku}

Registruoti Paraiskos Pozymi
    [Arguments]    ${ekoagros}    ${zyme}    ${param}
    SikuliLibrary.Wait Until Screen Contain    tvarkyti_paraiskas.png    20
    SikuliLibrary.Wait Until Screen Contain    blockPozymiai.png    20
    Click    blockPozymiai.png    yOffset=30
    Sleep    2s
    Press Special Key    F6
    SikuliLibrary.Paste Text    \    ${ekoagros}
    Sleep    1s
    Press Special Key    TAB
    Sleep    1s
    Run Keyword If    '${zyme}'!=''    SikuliLibrary.Paste Text    \    ${zyme}
    Press Special Key    TAB
    Sleep    1s
    Run Keyword If    '${param}'!='0'    SikuliLibrary.Paste Text    \    ${param}
    Press Special Key    F10
    Sleep    3s

Rasti Lauka
    [Arguments]    ${prs_zuikvc_id}    ${blokas}    ${laukas}    ${versert}    ${robsert}    ${robkl}
    ${query3} =    Get File    ${dbpath}\\u-laukas.sql
    @{query3Results} =    OracleDB.Execute Sql String    ${query3}    arg1=${prs_zuikvc_id}    arg2=${blokas}    arg3=${laukas}    arg4=${versert}    arg5=${robsert}    arg6=${robkl}
    Log Many    @{query3Results}
    ${q3status}    ${q3value} =    Run Keyword And Ignore Error    Should Not Be Empty    ${query3Results}
    [Return]    ${query3Results}

Tikrinti Laukus
    [Arguments]    ${prs_zuikvc_id}    ${blokai_laukai}    ${rows}    ${ekoagros}
    Create List    @{pastabos}
    FOR    ${row}    IN RANGE    0    ${rows}-1
        ${blokas} =    Set Variable    ${blokai_laukai[0][${row}]}
        ${laukas} =    Set Variable    ${blokai_laukai[1][${row}]}
        ${pastaba} =    Catenate    SEPARATOR=|    ${blokai_laukai[0][${row}]}    ${blokai_laukai[1][${row}]}    ${blokai_laukai[2][${row}]}    ${blokai_laukai[3][${row}]}    ${blokai_laukai[4][${row}]}
        Log    ${blokas}
        Log    ${laukas}
        @{query3Results} =    Rasti Lauka    ${prs_zuikvc_id}    ${blokas}    ${laukas}    ${versert}    ${robsert}    ${robkl}
        Log Many    @{query3Results}
        ${q3status}    ${q3value} =    Run Keyword And Ignore Error    Should Not Be Empty    ${query3Results}
        Run Keyword If    '${q3status}' == 'FAIL'    Append To List    ${pastabos}    ${pastaba}
    END
    ${len}    Get Length    ${pastabos}
    Run Keyword If    '${len}'!='0'    Run Keywords    Log    Sertifikuojamas laukas nerastas paraiškoje
    ...    AND    Log    ${pastabos}
    [Return]    ${pastabos}

Uzregistruoti Nagrinejima
    Click    btnNagrinejimai.png
    Wait Until Screen Contain    btnsUzregistruoti.png    30
    Click In    btnsUzregistruoti.png    btnUzregistruoti.png
    Wait Until Screen Contain    pranesimai_perspejimai.png    40
    ${status1}    ${value1} =    Run Keyword And Ignore Error    Screen Should Contain    btnTesti.png
    ${status2}    ${value2} =    Run Keyword And Ignore Error    Screen Should Contain    btnUzdaryti.png
    Run Keyword If    '${status1}'=='PASS'    Click    btnTesti.png
    Run Keyword If    '${status2}'=='PASS'    Click    btnUzdaryti.png
    Wait Until Screen Contain    btnsUzregistruoti.png    30
    Run Keyword If    '${status2}'=='PASS'    Run Keywords    Click In    btnsUzregistruoti.png    btnUzregistruoti.png
    ...    AND    Wait Until Screen Contain    pranesimai_perspejimai.png    30
    ...    AND    Click    btnTesti.png
    Run Keyword If    '${status2}'=='PASS'    Wait Until Screen Contain    btnsUzregistruoti.png    30
    Type With Modifiers    q    CTRL

Lauku Pastabos
    [Arguments]    @{pastabos}
    SikuliLibrary.Wait Until Screen Contain    tabs_prs.png    10
    SikuliLibrary.Click In    tabs_prs.png    tabPastabos.png
    SikuliLibrary.Wait Until Screen Contain    areaPastabos.png    10
    SikuliLibrary.Click    areaPastabos.png
    SikuliLibrary.Wait Until Screen Contain    areaPastabosAct.png    10
    Press Special Key    RIGHT
    ${pastaba} =    Catenate    Sertifikuojamas laukas nerastas paraiškoje    ${pastabos}
    SikuliLibrary.Paste Text    areaPastabosAct.png    ${pastaba}
    Press Special Key    F10

Open PPAIS
    Open Browser    ${urlPPAIS}    ie    alias=ppais
    Maximize Browser Window
    SikuliLibrary.Wait Until Screen Contain    warning.png    30
    SikuliLibrary.Click    btnCancel.png
    SikuliLibrary.Wait Until Screen Contain    vardas_ppais.png    40
    SikuliLibrary.Input Text    vardas_ppais.png    ${userPPAIS}
    Press Special Key    TAB
    SikuliLibrary.Input Text    \    ${passPPAIS}
    Press Special Key    TAB
    SikuliLibrary.Input Text    \    ${dbPPAIS}
    Press Special Key    ENTER
    SikuliLibrary.Wait Until Screen Contain    padaliniai_ppais.png    20
    Double Click    3skyrius.png
    SikuliLibrary.Wait Until Screen Contain    metai_ppais.png    40
    Set Ocr Text Read    true
    ${getMetai} =    SikuliLibrary.Get Text    get_metai.png
    Sleep    1s
    Run Keyword Unless    '${getMetai}'=='${metai}'    SikuliLibrary.Click    btnKeisti.png
    SikuliLibrary.Wait Until Screen Contain    security_warning.png    20
    SikuliLibrary.Click    security_no.png
    SikuliLibrary.Wait Until Screen Contain    nustatyti_metus.png    10
    Press Special Key    F7
    SikuliLibrary.Paste Text    \    ${metai}
    Press Special Key    F8
    Press Special Key    ENTER
    Press Special Key    ENTER
    SikuliLibrary.Wait Until Screen Contain    rasti_ppais.png    10
    SikuliLibrary.Paste Text    rasti_ppais.png    paraiškos
    SikuliLibrary.Click    btnRasti.png
    Press Special Key    ENTER
    Press Special Key    ENTER
    SikuliLibrary.Wait Until Screen Contain    paraiskos_ppais.png    10
    Double Click In    areaParaiskos.png    paraiskos_ppais.png
    Set Suite Variable    ${open_ppais}    1

Open AGROS
    Open Browser    ${UrlAgros}    chrome    alias=agros
    Wait Until Page Contains Element    name=username
    SeleniumLibrary.Input Text    name=username    ${userAgros}
    SeleniumLibrary.Input Password    name=password    ${passAgros}
    Click Button    //input[@value='Prisijungti']
    Set Suite Variable    ${open_agros}    1
