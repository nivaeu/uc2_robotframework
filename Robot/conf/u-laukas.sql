select
  dkl_id
  ,dkl_atsisake_paramos
  ,dkl_dekl_plotas
  ,kzs_numeris
  ,dkl_numeris
  ,psl_kodas  
  
  ,vs.adp_id                   vs_adp_id
  ,vs.adp_reiksme              vs_adp_reiksme
  ,vs.adp_parametras           vs_adp_parametras
  ,vs.adp_pastaba              vs_adp_pastaba
  ,(select pzm_pavadinimas 
  from pp_pozymiai 
  where pzm_kodas = :arg4 
  and pzm_met_id = psl_met_id) vs_pav
  
  ,rs.adp_id                   rs_adp_id
  ,rs.adp_reiksme              rs_adp_reiksme
  ,rs.adp_parametras           rs_adp_parametras
  ,rs.adp_pastaba              rs_adp_pastaba
  ,(select pzm_pavadinimas
  from pp_pozymiai 
  where pzm_kodas = :arg5 
  and pzm_met_id = psl_met_id) rs_pav
  
  ,rk.adp_id                   rk_adp_id
  ,rk.adp_reiksme              rk_adp_reiksme
  ,rk.adp_parametras           rk_adp_parametras 
  ,rk.adp_pastaba              rk_adp_pastaba  
  ,(select pzm_pavadinimas 
  from pp_pozymiai 
  where pzm_kodas = :arg6 
  and pzm_met_id = psl_met_id) rk_pav
  
  from zuikvc_laukai_mv, pp_deklaruoti_laukai, pp_paseliai, pp_kontr_zemes_sklypai
  ,(select * from pp_administruojami_pozymiai, pp_pozymiai 
    where adp_pzm_id = pzm_id and pzm_kodas = :arg4) vs
  ,(select * from pp_administruojami_pozymiai, pp_pozymiai 
    where adp_pzm_id = pzm_id and pzm_kodas = :arg5) rs
  ,(select * from pp_administruojami_pozymiai, pp_pozymiai 
    where adp_pzm_id = pzm_id and pzm_kodas = :arg6) rk 
  where dkl_zuikvc_id = zdk_dkl_zuikvc_id and zdk_data_iki is null 
    and dkl_psl_id = psl_id and dkl_kzs_id = kzs_id 
    and zdk_prs_zuikvc_id = to_number(:arg1) 
    and zdk_kzs_numeris   =           :arg2
    and zdk_numeris       =           :arg3
    and dkl_id = vs.adp_dkl_id(+)
	and dkl_id = rs.adp_dkl_id(+)
	and dkl_id = rk.adp_dkl_id(+)