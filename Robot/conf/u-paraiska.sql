select 
  prs_id
  ,prs_numeris
  ,prs_busena
  ,to_date(PRS_REGISTRAVIMO_DATA, 'YYYY-MM-DD') PRS_REGISTRAVIMO_DATA
  ,prs_pad_id  
  ,pad_pad_id
  ,prs_drp_id 
  ,prs_zuikvc_id  
  ,(select 
     max(dok_dvs_nuoroda) keep (dense_rank last order by dok_registravimo_data) 
    from pp_dokumentai, pp_dokumentu_tipai 
    where dok_prs_id = prs_id 
    and dok_dtp_id = dtp_id 
    and dtp_kodas = 'A10'
    and dok_busena != 'N'
    ) dok_nuoroda
    ,'N' prisiskirti	
   ,adp_id	
   ,adp_reiksme
   ,adp_parametras
   ,met_metai
   ,prs_pastabos
   ,(select 1 from pp_paraisku_programos, pp_programos where ppg_prs_id = prs_id and ppg_panaikinta is null and ppg_pgr_id = pgr_id and pgr_kodas = 'EKO') eko
   ,(select 1 from pp_paraisku_programos, pp_programos where ppg_prs_id = prs_id and ppg_panaikinta is null and ppg_pgr_id = pgr_id and pgr_kodas = 'EKOP') ekop
   ,(select 1 from pp_paraisku_programos, pp_programos where ppg_prs_id = prs_id and ppg_panaikinta is null and ppg_pgr_id = pgr_id and pgr_kodas = 'EKOV') ekov
   
   ,(select 1 from pp_paraisku_sgv, pp_sgv_programos where psg_prs_id = prs_id and psg_spr_id = spr_id and spr_numeris = 'EKOSGV') ekosgv
   ,(select 1 from pp_paraisku_sgv, pp_sgv_programos where psg_prs_id = prs_id and psg_spr_id = spr_id and spr_numeris = 'EKOL2SGV') ekol2sgv
   ,(select 1 from pp_paraisku_sgv, pp_sgv_programos where psg_prs_id = prs_id and psg_spr_id = spr_id and spr_numeris = 'EKOL5SGV') ekol5sgv
   
from pp_pareiskejai, pp_metai, pp_paraiskos, pp_padaliniai,
  (select * from pp_pozymiai, pp_administruojami_pozymiai
   where adp_pzm_id = pzm_id 
   and pzm_kodas = :arg3
   )
where par_valdos_numeris = :arg1
  and met_metai = :arg2
  and prs_par_id = par_id 
  and prs_met_id = met_id
  and prs_pad_id = pad_id
  and prs_id = adp_prs_id(+)