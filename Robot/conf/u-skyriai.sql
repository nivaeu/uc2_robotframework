select 
drb_naudotojo_vardas,
  drb_id
 ,drp_id
 ,pad_pavadinimas 
 ,pad_id          
from pp_padaliniai, pp_darbuotojai_padaliniuose, pp_darbuotojai
where pad_id = drp_pad_id and drp_drb_id = drb_id 
  and sysdate between drp_data_nuo and nvl(drp_data_iki, to_date('2222.01.01','YYYY.MM.DD'))
  and pad_id !=1 and drp_panaikinta is null
  and drb_naudotojo_vardas = upper('adm_ppais')